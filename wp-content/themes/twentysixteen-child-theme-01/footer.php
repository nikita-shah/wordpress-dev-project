<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
<div id="footer-background">
    
        <img id="footer-logo" class="alignnone size-full wp-image-71" src="<?php the_field('footer_logo') ?>" alt="LIPSUM Logo" />
        <h2>
            <div id="inner-text" class="inner-content">
                <div style="color: #fff;">
                    <?php the_field('footer_text') ?>
                </div>
            </div>
        </h2>
    
    <div class="button4" onclick="myFunction()">
        <?php the_field('footer_button') ?>
    </div>
    &nbsp;
</div>

<div class="button1" onclick="ClickMe()">
  <?php the_field('click_me') ?>
</div>

<footer id="colophon" class="site-footer" role="contentinfo" >

			<div class="site-info  footer-style">
			  <?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
			  
			  <a href="<?php echo esc_url( __( 'http://rockagency.com.au/', 'twentysixteen' ) ); ?>">
				  <?php printf( __( '© Copyright Rock Agency 2016  ')); ?>
			  </a>

			  <a href="<?php echo esc_url( __( '#', 'twentysixteen' ) ); ?>">
				 <?php printf( __( '&nbsp;&nbsp;Terms & Conditions  |  Privacy' )); ?>
			  </a>

			 <?php printf( __( '&nbsp;&nbsp;Website by&nbsp;' )); ?>

			  <a href="<?php echo esc_url( __( 'http://rockagency.com.au/', 'twentysixteen' ) ); ?>">
				 <?php printf( __( 'Rock Agency.' )); ?>
			  </a>

  			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
<?php the_field('footer_script') ?>
</body>
</html>
